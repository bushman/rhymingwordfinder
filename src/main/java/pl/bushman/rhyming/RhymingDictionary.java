package pl.bushman.rhyming;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

class RhymingDictionary {
    private CharNode charTree = new CharNode();

    RhymingDictionary(List<String> availableWords) {
        availableWords.forEach(this::addWord);
    }

    List<String> getRhymingWords(String word) {
        char[] wordChars = word.toCharArray();

        Stack<CharNode> visitedNodes = new Stack<>();
        visitedNodes.add(charTree);
        for (int wordCharsIndex = wordChars.length - 1; wordCharsIndex > -1; wordCharsIndex--) {
            char wordChar = wordChars[wordCharsIndex];
            CharNode subTree = visitedNodes.peek().get(wordChar);
            if (subTree == null) {
                break;
            }
            visitedNodes.add(subTree);
        }

        if (noRhymeFound(visitedNodes)) {
            return new ArrayList<>();
        }

        if (foundExactWord(visitedNodes)){
            popExactWordOnlyNodes(visitedNodes);
        }

        String longestSuffix = word.substring(word.length() - (visitedNodes.size() - 1));
        return getWords(visitedNodes.peek(), longestSuffix, word);

    }

    private void addWord(String word) {
        char[] wordChars = word.toCharArray();

        CharNode subTree = charTree;
        for (int wordCharsIndex = wordChars.length - 1; wordCharsIndex > -1; wordCharsIndex--) {
            char wordChar = wordChars[wordCharsIndex];
            CharNode newSubTree = subTree.get(wordChar);
            if (newSubTree == null) {
                newSubTree = new CharNode();
                subTree.put(wordChar, newSubTree);
            }
            subTree = newSubTree;
        }

        subTree.markAsWord();
    }

    private List<String> getWords(CharNode subTree, String suffix, String word) {
        List<String> result = new ArrayList<>();

        findWords(suffix, subTree, result);
        result.remove(word);

        return result;
    }

    private void findWords(String suffix, CharNode subTree, List<String> result) {
        if (subTree.isWord()) {
            result.add(suffix);
        }
        subTree.forEach((charr, newSubTree) ->
                findWords(charr + suffix, newSubTree, result));

    }

    private void popExactWordOnlyNodes(Stack<CharNode> visitedNodes) {
        visitedNodes.pop();
        while (hasNoChildrenButExactWord(visitedNodes.peek())){
            visitedNodes.pop();
        }
    }

    private boolean hasNoChildrenButExactWord(CharNode node) {
        return node.hasSingleChild();
    }

    private boolean foundExactWord(Stack<CharNode> visitedNodes) {
        return visitedNodes.peek().isChildless();
    }

    private boolean noRhymeFound(Stack<CharNode> visitedNodes) {
        return visitedNodes.size() == 1;
    }
}
