package pl.bushman.rhyming;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class RhymingWordFinder {
    private RhymingDictionary rhymingDictionary;

    public RhymingWordFinder(RhymingDictionary rhymingDictionary) {
        this.rhymingDictionary = rhymingDictionary;
    }

    public String findRhymingWord(String wordToRhyme) {
        List<String> rhymingWords = rhymingDictionary.getRhymingWords(wordToRhyme);

        return chooseWord(rhymingWords);
    }

    private String chooseWord(List<String> rhymingWords){
        if (rhymingWords.isEmpty()) {
            return null;
        } else if (rhymingWords.size() == 1) {
            return rhymingWords.get(0);
        } else {
            return randomWord(rhymingWords);
        }
    }

    private String randomWord(List<String> rhymingWords) {
        int randomWordIndex = ThreadLocalRandom.current().nextInt(0, rhymingWords.size());
        return rhymingWords.get(randomWordIndex);
    }
}
