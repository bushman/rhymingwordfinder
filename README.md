# RhymingWordFinder

input is a single word and must return the rhyming word that best matches from another list of words. The best rhyming
match is defined to be the one that has the most matching characters at the end of the string, ties should be broken randomly.