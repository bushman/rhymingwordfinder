package pl.bushman.rhyming;

import java.util.*;
import java.util.function.BiConsumer;

class CharNode {
    private final Map<Character, CharNode> tree = new HashMap<>();
    private boolean isWord = false;

    CharNode get(char charr) {
        return tree.get(charr);
    }

    void put(char charr, CharNode subTree) {
        tree.put(charr, subTree);
    }

    void forEach(BiConsumer<Character, CharNode> consumer) {
        tree.forEach(consumer);
    }

    boolean isWord() {
        return isWord;
    }

    void markAsWord() {
        isWord = true;
    }

    public boolean hasSingleChild() {
        return tree.size()==1;
    }

    public boolean isChildless() {
        return tree.size()==0;
    }
}
