package pl.bushman.rhyming;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class RhymingWordFinderTest {

    private String[] availableWords = {"Computing", "Polluting", "Diluting", "Commuting", "Recruiting", "Drooping", "ooping", "ping"};

    private final RhymingWordFinder rhymingWordFinder = new RhymingWordFinder(
            new RhymingDictionary(Arrays.asList(availableWords)));

    @Test
    public void findsExactlyOneWord() {
        String wordToRhyme = "Disputing";
        String expectedRhymingWord = "Computing";

        String resultRhymingWord = rhymingWordFinder.findRhymingWord(wordToRhyme);

        assertEquals(expectedRhymingWord, resultRhymingWord);
    }

    @Test
    public void findsMutipleWords1() {
        String wordToRhyme = "Shooting";
        String[] expectedRhymingWords = {"Computing", "Polluting", "Diluting", "Commuting", "Recruiting"};

        findsMutipleWords(wordToRhyme, expectedRhymingWords);
    }

    @Test
    public void findsMutipleWords2() {
        String wordToRhyme = "Convoluting";
        String[] expectedRhymingWords = {"Polluting", "Diluting"};

        findsMutipleWords(wordToRhyme, expectedRhymingWords);

    }

    @Test
    public void findsNoWordsWhenNoneRhyme() {
        String wordToRhyme = "Orange";

        assertNull(rhymingWordFinder.findRhymingWord(wordToRhyme));
    }

    @Test
    public void findsNoWordsWhenEmptyWord() {
        String wordToRhyme = "";

        assertNull(rhymingWordFinder.findRhymingWord(wordToRhyme));
    }

    @Test
    public void findsWordWhenRhymesWithSuffixOfAnotherWordInDictionary() {
        String wordToRhyme = "sleeping";
        String[] expectedRhymingWords = {"Drooping", "ooping", "ping"};

        findsMutipleWords(wordToRhyme, expectedRhymingWords);
    }

    @Test
    public void findsWordWhenSuffixOfAnotherWord() {
        String wordToRhyme = "ping";
        String[] expectedRhymingWords = {"Drooping", "ooping"};

        findsMutipleWords(wordToRhyme, expectedRhymingWords);
    }

    @Test
    public void ignoringWordEqualsToInputWord() {
        String wordToRhyme = "Polluting";
        String expectedRhymingWord = "Diluting";

        String resultRhymingWord = rhymingWordFinder.findRhymingWord(wordToRhyme);

        assertEquals(expectedRhymingWord, resultRhymingWord);

    }

    private void findsMutipleWords(String wordToRhyme, String[] expectedRhymingWords) {
        Set<String> foundWords = new HashSet<>();
        for (int i = 0; i < 10000; i++) {
            String resultRhymingWord = rhymingWordFinder.findRhymingWord(wordToRhyme);
            foundWords.add(resultRhymingWord);
            if (foundWords.size() == expectedRhymingWords.length) {
                break;
            }
        }

        List<String> expectedRhymingWordsList = new ArrayList<>(Arrays.asList(expectedRhymingWords));
        expectedRhymingWordsList.removeAll(foundWords);
        assertTrue(expectedRhymingWordsList.isEmpty(), "Expected words not found: " + expectedRhymingWordsList.toString());

        foundWords.removeAll(Arrays.asList(expectedRhymingWords));
        assertTrue(foundWords.isEmpty(), "Unexpected words found: " + foundWords.toString());

    }

}
